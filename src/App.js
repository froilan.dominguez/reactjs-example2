import React, { Component } from 'react';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userName: 'Adam',
      items: [
        { action: 'Buy Flowers', done: false  },
        { action: 'Get Shoes', done: false  },
        { action: 'Collect Tickets', done: true  },
        { action: 'Call Joe', done: false  }
      ],
      newItemText: ''
    };
  }

  updateNewTextValue = (event) => {
    this.setState({ newItemText: event.target.value });
  }

  createNewItem = () => {
    if (!this.state.items.find(item => item.action === this.state.newItemText)) {
      this.setState({
        items: [
          ...this.state.items,
          { action: this.state.newItemText, done: false }
        ],
        newItemText: ''
      });
    }
  }

  toggle = (itm) => this.setState({ 
    items: this.state.items.map(item => item.action === itm.action ? {...item, done: !item.done} : item)
  });

  tableRows = () => this.state.items.map(item => 
    <tr key={item.action}>
      <td>{item.action}</td>
      <td>
        <input type="checkbox" checked={item.done} onChange={() => this.toggle(item)}/>
      </td>
    </tr>);

  render() {
    return (
      <div>
        <h4 className="bg-primary text-white text-center p-2">
          {this.state.userName}'s To Do List ({this.state.items.filter(t => !t.done).length}) items to do
        </h4>
        <div className="container-fluid">
          <div className="my-1">
            <input className="form-control" value={this.state.newItemText} onChange={this.updateNewTextValue}></input>
            <button className="btn btn-primary m-2" onClick={ this.createNewItem }>
              Change
            </button>
          </div>
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Description</th>
                <th>Done</th>
              </tr>
            </thead>
            <tbody>{this.tableRows()}</tbody>
          </table>
        </div>
      </div> 
    );
  }
}